# Puppet ENC

## Introduction

This repository provides External Node Classifier (ENC) data, using Ant tasks
to translate service-based data stored within this repository into per-node
data in a format that can be consumed on Puppet servers. The Control repository
that sits alongside this repository has support for this ENC built into its
`puppetserver` role.

Logically, each service is defined as a set of tiers, each of which contains a
list of servers. The Puppet environment can be set at the overall service
level, within a specific tier and for a specific server. More specific
definitions override more general definitions so an environment defined for 
an individual server takes precedence over the tier-defined enviroment, which
takes precendence over the service-defined environment.

In addition, ENC parameters can be defined at each level. Individual server
parameter values override values set at a more general level, in the same
manner as the environments.

The `tier` parameter is automatically set to the tier that each server appears
inside, allowing Hiera to reference `${::tier}` in its hierarchy.

## Parameters, Trusted Facts and Hiera Variables

Parameters defined within ENCs become available to Puppet code as variables
defined within the root scope. As such, they provide a method for passing data
into Puppet code along with the traditional use of Hiera variables and
the use of trusted facts baked into node certificates.

What is the role of parameters?

In practically every single use case, Hiera variables should be used to
pass data into Puppet code. However, there will be some use cases where:

* We want to associate data with individual servers
* We do not want to bake the data into the node certificate because it is volatile

In this case, ENC parameters provide a useful alternative to defining facts on
the servers themselves.

For example, if one server out of a group of servers within a tier needs to
be responsible for running overnight batch jobs, baking this into the node
certificate would be inflexible in cases where we need to move where the
batch jobs run. Hiera would also be inflexible because of the workflows used
to make changes and merge them into the production environment.

In this case it would be appropriate to define a parameter as a simple
switch within the ENC. For example:

    "service-kb-live": {
      "run_batch_jobs": true
    }

## Defining Tiers and Servers in Services

Services are defined as JSON files inside the `service/` directory. The
basic service definition is:

    {
      tiers: {}
    }

Individual tiers can be defined within the `tiers` property:

    {
      tiers: {
        "dev": {},
        "test": {},
        "live": {}
      }
    }

Servers can then be defined within each tier:

    {
      tiers: {
        "dev": {
          "servers": {
            "service-dev-kb.is.ed.ac.uk": {},
            "service-dev-at.is.ed.ac.uk": {},
          }
        },
        "test": {
          "servers": {
            "service-test-kb.is.ed.ac.uk": {},
            "service-test-at.is.ed.ac.uk": {},
          }
        },
        "live": {
          "servers": {
            "service-live-kb.is.ed.ac.uk": {},
            "service-live-at.is.ed.ac.uk": {},
          }
        },
      }
    }

## Defining Environments

The default environment for all of the servers defined within the service can
be set at the top level:

    {
      "environment": "production",
      "tiers": {
        "dev": {
          "servers": {
            "service-dev-kb.is.ed.ac.uk": {},
            "service-dev-at.is.ed.ac.uk": {},
          }
        },
        .
        .
      }
    }

To override the environment for a specific tier, add an `environment` property
to the tier definition. For example:

    {
      "environment": "production",
      "tiers": {
        "dev": {
          "environment": "feature-abc",
          "servers": {
            "service-dev-kb.is.ed.ac.uk": {},
            "service-dev-at.is.ed.ac.uk": {}
          }
        },
        .
        .
      }
    }

This will move all of the servers within the `dev` tier into the
`feature-abc` environment.

The environment on individual servers can also be changed. For example:

    {
      "environment": "production",
      "tiers": {
        "dev": {
          "servers": {
            "service-dev-kb.is.ed.ac.uk": {
              "environment": "feature-abc"
            },
            "service-dev-at.is.ed.ac.uk": {}
          }
        },
        .
        .
    }

In this case only `service-dev-kb` would move into the `feature-abc`
environment. The other server would remain in whichever environment is defined
for the overall service.

## Defining parameters

Parameters that apply to all of the servers defined within the service can
be set at the top level:

    {
      "environment": "production",
      "parameters": {
        "param1": "value1",
        "param2": "value2"
      },
      "tiers": {
        .
        .
      }
    }

Parameters can also be set at the tier level and at the individual server level:

    {
      "environment": "production",
      "parameters": {
        "param1": "value1",
        "param2": "value2"
      },
      "tiers": {
        "dev": {
          "parameters": {
            "param3": "value3"
          }
          "servers": {
            "service-dev-kb.is.ed.ac.uk": {
              "parameters": {
                "param1": "overridenvalue"
              }
            },
            "service-dev-at.is.ed.ac.uk": {}
          }
        },
        .
        .
      }
    }

In this example, `param3` would only be set for the two servers in the
`dev` tier and the parameter `param1` would be defined as `overridenvalue`
for `service-dev-kb` but would remain defined as `value1` for `service-dev-at`
and all other servers.
